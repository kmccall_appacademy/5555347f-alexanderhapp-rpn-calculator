class RPNCalculator
  attr_accessor :operands

  def initialize
    @operands = []
  end

  def push(number)
    operands.push(number)
  end

  def plus
    operate(:+)
  end

  def minus
    operate(:-)
  end

  def divide
    operate(:/)
  end

  def times
    operate(:*)
  end

  def operate(operator)
    empty_stack_error if @operands.empty?
    calculator = get_operands
    array_to_float(calculator) if operator == :/
    @operands.push(calculator.reverse.reduce(operator))
  end

  def get_operands
    calculator = []
    2.times do
      calculator << @operands.pop
    end
    calculator
  end

  def tokens(string)
    string.split(" ").map do |chr|
      chr.match(/[\+\-\*\/]/) ? chr.to_sym : chr.to_i
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      el.is_a?(Integer) ? push(el) : operate(el)
    end
    value
  end

  def value
    @operands.last
  end

  def array_to_float(arr)
    arr.map!(&:to_f)
  end

  def empty_stack_error
    raise "calculator is empty"
  end

end
